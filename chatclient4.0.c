/* Chat client that connects to a chat server on port 49153 at pilot.westmont.edu.
Enter a username then enter a message to send, "c" to see new messages, or "q" to quit the client.
Created by Ben Thomas for CS-140 1/25/19 */
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>

#define PORT 49153


void main(int argc, char *argv[]){
	
	// Initializing variables
	int socketDes;
	struct sockaddr_in serverAddr;
	char username[20], buffer [1024];

	
	// Create client socket + Error check
	if ((socketDes = socket(AF_INET,SOCK_STREAM,0)) < 0){
		printf("ERROR: failed to open socket\n");
		}

	// Create sock_addr structure for server adress
	memset(&serverAddr,'0',sizeof(serverAddr));
	serverAddr.sin_family = AF_INET; 
	serverAddr.sin_port = htons(PORT);
	if (inet_pton(AF_INET,"10.115.20.250", &serverAddr.sin_addr) < 0){
	// inet_pton test incase IP changes???
	printf("ERROR:inet_pton failed....Check for updated IP adress\n");}
	

	// Connect to chat server @ pilot.westmont.edu on port 49153 + Error check
	if (connect(socketDes,(struct sockaddr*)&serverAddr,sizeof(serverAddr)) < 0){
		printf("ERROR:failed to connect to server\n");
		}
	else{  
		printf("\nSuccessful connection\n");
		}
		

	// Get username 
	// note: username must be less than 20 characters
	printf("Username: ");
	scanf("%s", username);
	printf("Hi %s, I am connecting you to the chat server\n",username);


	// Send username as first line
	int x = send(socketDes,strcat(username,"\n"),strlen(strcat(username,"\n")),0);//send username
	if (x < 0) { printf("send successful %d",x);}

	// Read intial message from the server
	read(socketDes,buffer,1024);	
	("%s\n", &buffer);

	// TODO: create message ability | message refresher | client termination

	
	


	// Close connection + Error check
	if (close(socketDes) < 0){
		printf("ERROR:failed to closed socket\n");}
  
	
	 
	
}

