/* Chat client that connects to a chat server on port 49153 at pilot.westmont.edu .
Enter a username, type a message or press enter to see new messages. Enter "q" to quit the client.
Created by Ben Thomas for CS-140 1/25/19 */
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>

#define PORT 49153


void main(){
	
	// Initializing variables
	int socketDes;
	struct sockaddr_in serverAddr;
	char buffer [256], userinput[80], username[20];

	
	// Create client socket + Exception
	if ((socketDes = socket(AF_INET,SOCK_STREAM,0)) < 0){
		printf("ERROR: failed to open socket\n");
		goto exit;
	}
	
	// Create sock_addr structure for server adress
	memset(&serverAddr,'0',sizeof(serverAddr));
	serverAddr.sin_family = AF_INET; // Select IPv4
	serverAddr.sin_port = htons(PORT); // Specify server port
	if (inet_pton(AF_INET,"10.115.20.250", &serverAddr.sin_addr) < 0){// Convert IP to binary
		// inet_pton Exception incase IP changes???
		printf("ERROR:inet_pton failed....Check for updated IP adress\n");
	}
	

	// Connect to chat server @ pilot.westmont.edu on port 49153 + Exception
	if (connect(socketDes,(struct sockaddr*)&serverAddr,sizeof(serverAddr)) < 0){
		printf("ERROR:failed to connect to server\n");
		goto exit;
	}
	

	// Get username 
	// note: username must be less than 20 characters
	printf("Username: ");
	fgets(username,19,stdin);


	// Send username as first line + Exception
	
	if (send(socketDes,username,strlen(username),0) < 0) { 
		printf("ERROR:failed to send to server\n");
	}


	// Remove "\n" from username for formating
	strtok(username, "\n"); 
printf("\ngot to here\n");


	// Read intial message from server + Exception
	if (read(socketDes,buffer,255) < 0){
		printf("ERROR:failed to read from server\n");
		goto exit;
	}
	printf("%s\n", buffer); 
	memset(buffer,0,strlen(buffer));// Clear buffer
	

	// Print user instructions
	printf("Press enter to view recent messages, and \"q\" to quit\n\n");
	printf("%s- ",username);// Message prompt

	// User interaction 
	while (1){
		fgets(userinput,79,stdin);// Read user input
		if (strcmp(userinput,"q\n") == 0){// Terminate with "q"
			goto exit;
		}
		else if (strcmp(userinput,"\n") == 0){// View messages with Enter
			read(socketDes,buffer,255);
			printf("%s\n", buffer);
			memset(buffer,0,strlen(buffer));
		}
		else {
			send(socketDes,userinput,strlen(userinput),0);// send user message
			read(socketDes,buffer,255);
			printf("%s\n", buffer);
		}
		memset(userinput,0,strlen(userinput));
	}

	


// TODO: create message ability | message refresher |

	
	
	// Close socket then terminate client + Exception
	exit:
	if (close(socketDes) == 0){
		printf("\nGoodbye\n");
	}
	else {
		printf("ERROR:failed to close socket");
	}
	 
}

